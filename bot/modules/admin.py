import discord
from discord.ext import commands
from bot import config
from bot import db

class Admin(commands.Cog): # TODO Security 
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name=".leave")
    async def puts(self, ctx, *args):
        leave = ctx.guild
        if ctx.message.author.id in config['shutdown_users']:
            await ctx.send("Goodbye.")
            await leave.leave()



def setup(bot):
    bot.add_cog(Admin(bot))


