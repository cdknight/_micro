import discord
from discord.ext import commands

from bot import db

class Roles(commands.Cog): # TODO Security 
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name=".addrole")
    async def addrole(self, ctx, arg):
        guild = ctx.message.guild
        await guild.create_role(name=arg)
        await ctx.send(f"{arg} role added!")

    @commands.command()
    async def listroles(self, ctx, settable=""):
        guild = ctx.message.guild
        roles = guild.roles

        role_embed = discord.Embed(title="Roles")
        role_names = ''

        allowed_roles = db['allowed_roles']

        for role in roles:
            if settable == "settable":
                role_embed.title = "Settable Roles"
                if role.name in allowed_roles['allowed_roles']:
                    role_names += f"`{role.name}`\n"
            else:
                    role_names += f"`{role.name}`\n"
        role_embed.add_field(name="Roles List", value=role_names)
        await ctx.send(embed=role_embed)

    @commands.command()
    async def togglerole(self, ctx, role):
        guild = ctx.message.guild
        rolenames = [role.name for role in guild.roles]

        if role in rolenames:
            roles_set_db = 'allowed_roles' in db

            if not roles_set_db:
                data = {
                    '_id': 'allowed_roles',
                    'allowed_roles': {}
                }
                db.create_document(data)


            allowed_roles = db['allowed_roles']
            if not allowed_roles.get(guild.id):
                allowed_roles[guild.id] = []

            if role not in allowed_roles[guild.id]:
                allowed_roles[guild.id].append(role)

                allowed_roles.save()
                await ctx.send(f"Role {role} now settable by users.")
            else:
                del allowed_roles[guild.id][allowed_roles[guild.id].index(role)]
                allowed_roles.save()
                await ctx.send(f"Role {role} now not settable by users.")
        else:
            await ctx.send("Role doesn't exist!")


    @commands.command()
    async def giverole(self, ctx, role):
        guild = ctx.message.guild
        rolenames = guild.roles
        member = ctx.message.author

        allowed_roles = db['allowed_roles'][guild.id]

        if role in [role.name for role in rolenames]:
            if role in allowed_roles:
                for role_obj in rolenames:
                    if role_obj.name == role:
                        await member.add_roles(role_obj)
                        await ctx.send(f"Added role `{role}` succesfully!")
            else:
                await ctx.send("This role is not self-assignable.")
        else:
            await ctx.send(f"Role `{role}` doesn't exist!")


def setup(bot):
    bot.add_cog(Roles(bot))


