import discord
from discord.ext import commands
from bot import config

class Basics(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def test(self, ctx):
        testembed = discord.Embed(title="Hello! :partying_face:", description="I'm a test embed!")
        await ctx.send(embed=testembed)

    @commands.command()
    async def puts(self, ctx, *args):
        await ctx.send(" ".join(args))

    @commands.command()
    async def help(self, ctx):
        prefix = self.bot.command_prefix

        help_embed = discord.Embed(title="Help")

        cogs = self.bot.cogs
        for cog_name, cog in cogs.items():
            cog_command_value = ""
            for command in cog.get_commands():
                cog_command_value += f"{prefix}{command.name}\n"
            if len(cog.get_commands()) == 0:
                cog_command_value = "Empty :("
            help_embed.add_field(name=cog_name, inline=True, value=cog_command_value)

        await ctx.send(embed=help_embed)

    @commands.command()
    async def setprefix(self, ctx, newprefix):
        self.bot.command_prefix = newprefix
        await ctx.send(f"Prefix updated to {newprefix}.")

    @commands.command()
    async def reloadcog(self, ctx, cogname):
        self.bot.reload_extension(cogname)
        await ctx.send(f"Module {cogname} reloaded successfully!")

    @commands.command()
    async def apoptosis(self, ctx):
        if ctx.message.author.id in config['shutdown_users']:
            await ctx.send("Bye!");
            await self.bot.change_presence(status=discord.Status.invisible)
            await self.bot.logout()

def setup(bot):
    bot.add_cog(Basics(bot))
