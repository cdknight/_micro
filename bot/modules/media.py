import discord
from discord.ext import commands

from bot import db
import youtube_dl
import os


class Media(commands.Cog): # TODO Security 
    def __init__(self, bot):
        self.bot = bot
        self.ytdl = youtube_dl.YoutubeDL({
        })

    @commands.command()
    async def play(self, ctx, link):
        guild = ctx.guild
        vc = ctx.voice_client

        if not vc:
            voice_channel = ctx.message.author.voice.channel
            vc = await voice_channel.connect()


        os.makedirs(f"downloads/{guild.id}", exist_ok=True)
        self.ytdl.params = {"outtmpl": f"downloads/{guild.id}/%(title)s.%(ext)s"}
        self.ytdl.download([link])

        video_info = self.ytdl.extract_info(link, download=False)
        title = video_info.get('title')
        ext = video_info.get('ext')

        audiosource = discord.FFmpegPCMAudio(os.path.join(os.getcwd(), f"downloads/{guild.id}/{title}.{ext}"))
        print(audiosource == None)
        vc.play(audiosource)

        await ctx.send(f"Playing {title} in the voice channel you're connected to!")

    @commands.command()
    async def stop(self, ctx):
        vc = ctx.voice_client

        if not vc:
            await ctx.send("Bot is not playing anything!")

        await vc.disconnect()



def setup(bot):
    bot.add_cog(Media(bot))


