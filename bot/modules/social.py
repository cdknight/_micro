import discord
from discord.ext import commands

from bot import db

class Social(commands.Cog): # TODO Security 
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def emojitype(self, ctx, *argtext):
        text = ' '.join(argtext)
        emojistr = ''
        for i in text:
            if i != ' ':
                emojistr += f":regional_indicator_{i}: "
            elif i == "!":
                emojistr += f":exclamation: "
            elif i == "?":
                emojistr += f":question: "
            else:
                emojistr += "  "
        await ctx.send(emojistr)


def setup(bot):
    bot.add_cog(Social(bot))



