from cloudant.client import CouchDB
import os
import json

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.json")) as cf:
        config = json.load(cf)
        couch_user = config.get("couchdb_username")
        couch_password = config.get('couchdb_password')
        couch_url = config.get('couchdb_url')
        couch_dbname = config.get("couchdb_dbname")

couchdb_client = CouchDB(
        couch_user,
        couch_password,
        url=couch_url,
        connect=True,
        auto_renew=True
)

db = couchdb_client[couch_dbname]
