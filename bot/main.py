import discord
from discord.ext import commands
import json
from bot import config

bot = commands.Bot(command_prefix=".")

@bot.event
async def on_ready():
    print("hi")
    game = discord.Game("Serving my masters dutifully")
    await bot.change_presence(status=discord.Status.idle, activity=game)

@bot.event
async def on_member_join(member):
    await member.guild.system_channel.send(f"Welcome, <@{member.id}>!")

@bot.event
async def on_member_remove(member):
    await member.guild.system_channel.send(f"Farewell, <@{member.id}>.")

bot.remove_command('help')

bot.load_extension("bot.modules.basics")
bot.load_extension("bot.modules.roles")
bot.load_extension("bot.modules.social")
bot.load_extension("bot.modules.media")
bot.load_extension("bot.modules.admin")



bot.run(config['bot_token'])
